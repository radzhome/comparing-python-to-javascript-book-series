# Comparing Python to Javascript (book series)

This is a series of books that compare the core mechanisms of the JavaScript and Python language.


**Check out the [Preface](preface.md).**

## Titles

* Read online: ["Introduction"](intro/README.md#comparing-python-to-javascript--intro)

## License & Copyright

The materials herein are all (c) 2013-2015 Radomir Wojcik, Steven Smith.

<a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/3.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/3.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/3.0/">Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License</a>.

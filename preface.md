# Comparing Python to Javascript
# Preface

You know Python or Javascript fairly well but you want to explore the internals of the language and how it differs from other languages.

You are confident with Python or Javascript, but don't know where to start when learning one of these languages for the first time.

TODO

## Mission

This book will cover a lot of material with as little words as possible but enough to make distinctions clear.

Often times own research will be required to dive down into the deeper mechanisms of the respective languages.

TODO

## Summary

A comparison of the two of the most popular programming languages used in the software industry today.

TODO